__author__ = 'Dhanraj Akula'
__date__ = 'Oct 15 2016'



import numpy as np

def ismember(a,b):
  #deleting all the nan from the b matrix so that it reduces the computation  
  b=np.matrix(np.delete(b,np.where(np.isnan(b[:,0])),axis=0).astype(int))
  member_in_a=[]
  index_in_b=[]
  count_b=0
  check=0
  for i in a : 
    for j in b:
      count_b=count_b+1
      if np.array_equal(j,i):
        check=1
        member_in_a.append(1)
        index_in_b.append(count_b-1)
        break
      else:
        check=0
    count_b=0
    
    if check==0:
      index_in_b.append(-1)
      member_in_a.append(0)

  return {'member_in_a':member_in_a,'index_elements_b':index_in_b}




  #Starting of the code for Astar algorithm 


#def Astar(relPose,obGrid,targetPt):
#Defining the List Size 

def astar(nodes,obGrid,relPose,targetPt):

	m=obGrid.shape[0]
	n=obGrid.shape[1]
	GridPts=m*n
	#Initializing a path output variable 
	Path= np.empty((GridPts,2))
	Path[:] = np.NAN
	#Initializing variable to track is a solution has been found 
	solution_found=0;
	#tarcking the XY cordinate of each point to be evaluated 
	open_list_XY=np.empty((GridPts,2))
	open_list_XY[:]=np.NAN
	# open_list_XY=np.matrix(open_list_XY)

	#track teh calculation F,G,H weights (where G is the movement cost to get to the )
	#point ,H is the hypothetical cost t reach teh target from that point before the full 
	#path is evaluated ,and F is the sum of both G and H for the point being evaluated 
	open_list_FGH=np.empty((GridPts,3))
	open_list_FGH[:]=np.NAN

	#track the cordinates of the parent for each of the points being evaluted 
	open_list_parent= np.empty((GridPts,2))
	open_list_parent[:]=np.NAN

	#track the XY coordinate of each point that has been fully evaluated 
	closed_list_XY=np.empty((GridPts,2))
	closed_list_XY[:]=np.NAN
	# closed_list_XY=np.matrix(closed_list_XY)

	#Track the parent coordinate of each point that has been fully evaluted 
	closed_list_parent= np.empty((GridPts,2))
	closed_list_parent[:]=np.NAN

	#relative indices to neighbours points of the square,to be used for 
	#evaluating potential candidate for the open list
	neighbours_XY=np.matrix([ [-1,-1],[-1,0],[-1,1],[0,1],[1,1],[1,0],[1,-1],[0,-1] ])
	neighbours_G=np.matrix([ [14],[10],[14],[10],[14],[10],[14],[10] ])

	#Make the current pose (in the relative coordinates of the feild) the first point in the open list 
	open_list_XY[0,:]=relPose
	#set the movement cost(G) of this oint to zero 
	open_list_FGH[0,1]=0;
	#Calculate the hypothetical cost(H) 
	open_list_FGH[0,2]=10*(abs( targetPt[0]-relPose[0] ) + abs(targetPt[1]-relPose[1]) )
	#Sum is the full cost (F=G+H)
	open_list_FGH[0,0]=open_list_FGH[0,1] + open_list_FGH[0,2]


	#Creating a list of invalid points 
	#Obstacles are invalid points 
	row=np.where(obGrid == -1)[0]
	col=np.where(obGrid == -1)[1]
	#outside the edge are invalid 
	left=np.transpose(np.concatenate( ( np.matrix([range(-1,m+1)][0]) , -1*	np.ones((1,m+2) ) ),axis=0) )
	right=np.transpose(np.concatenate( ( np.matrix([range(-1,m+1)][0]) , (n)*np.ones((1,m+2) ) ),axis=0) )
	bottom =np.transpose(np.concatenate( ( -1*np.ones((1,n) ),np.matrix([range(0,n)][0])) ,axis=0))
	top=np.transpose(np.concatenate( ( (m)*np.ones((1,n) ),np.matrix([range(0,n)][0])) ,axis=0))

	obs_index=np.transpose(np.matrix([row,col]))
	#the matrix which contains invalid sets of index 
	invalid_pts=np.concatenate((obs_index, left,right,bottom,top),axis=0).astype(int)
	# count=0
	while np.sum(np.invert(np.isnan(open_list_XY[:,0]))) >=1:
		# count=count+1
		# print 'Current count in the while loop:',count
		#Find the lowest F cost square on the open list,checking newer entries over older 
		#this is done by fipping the list up/down to get newer  entries to evaluate first 
		#this is marked as the current square 
		flipud_open_list_FGH=np.flipud(open_list_FGH[:,0])
		min_open_list_F=np.nanmin(flipud_open_list_FGH,axis=0)
		idxI =np.where(flipud_open_list_FGH == min_open_list_F)
		currentSquare=(GridPts-1)-idxI[0][0]
		
		current_XY=open_list_XY[currentSquare,:].astype(int)
		current_G=open_list_FGH[currentSquare,1].astype(int)

		#Find the first empty element in the closed list and replace the place holder with 
		#the information for the current square 
		emptyClosed=np.where(np.isnan(closed_list_XY[:,0])==1)
		closed_list_XY[emptyClosed[0][0],:]=current_XY.astype(int)
		closed_list_parent[emptyClosed[0][0],:]=open_list_parent[currentSquare,:]
		#Remove the current square from the open list now that it is in the closed list 
		open_list_XY[currentSquare,:]='nan'
		open_list_FGH[currentSquare,:]='nan'
		open_list_parent[currentSquare,:]='nan'
		#if the current point that just got added to the closed list is the target point 
		#exit the loop because a complete path can be traced 

		if np.array_equal(current_XY,targetPt):
			solution_found=1
			print 'Solution found'
		 	break

		#checking the adjacent squares 
		#create a list of points around the current point and check their G value 
		rel_list_XY=np.concatenate((neighbours_XY[:,0]+current_XY[0],neighbours_XY[:,1]+current_XY[1]),axis=1)
		rel_list_G=(neighbours_G+current_G)
		#check walkable /on the closed list 
		#remove the members of the relative list if on closed list 
		
		#function performs similar operation to that of of ismember(a,b,'rows') 
		#first checking if the elements in colums of b exsists in the colums of a respectively 
		#then performing logical and operation between the corresponding elemnts in colums 
		#if that results in true then there is a identical row in a and b.
		
		on_closed=ismember(rel_list_XY,(closed_list_XY))['member_in_a']
		rel_list_XY=np.delete(rel_list_XY,[j for j, x in enumerate(on_closed) if x == 1],axis=0)
		
		rel_list_G=np.delete(rel_list_G,[j for j, x in enumerate(on_closed) if x == 1],axis=0)
		#check to see if members of relative list are either obstacles or out of bound 
		
		invalid=ismember(rel_list_XY,invalid_pts)['member_in_a']
		# print 'np.where(invalid == 1)',np.where(invalid == 1)
		# rel_list_XY=np.delete(rel_list_XY,np.where(invalid == 1),axis=0)
		rel_list_XY=np.delete(rel_list_XY,[j for j, x in enumerate(invalid) if x == 1],axis=0)

		# rel_list_G=np.delete(rel_list_G,np.where(invalid == 1),axis=0)
		rel_list_G=np.delete(rel_list_G,[j for j, x in enumerate(invalid) if x == 1],axis=0)
		
		#calculating the F and H and then add penalty weights to G scores 

		rel_list_H=10*(abs(targetPt[0]-rel_list_XY[:,0])+abs(targetPt[1]-rel_list_XY[:,1]))
		
		#if the matrix rel_list_XY is empty then then rel_list_XY.size=0
		#since we want to equate it to not empty condition checking if the size is 1
		#which effectively is checking for not empty condition 
		#if the rel_list_XY is empty then empty_rel_list_XY=0 else empty_rel_list_XY=1

		#defining a variable linear_obGrid which is a convertion of obGrid of m*n matrix into 1,m*n 

		if np.invert(rel_list_XY.size==0):
			
			
			rel_list_G=rel_list_G + 20*obGrid[ rel_list_XY[:,0], rel_list_XY[:,1] ]
			
			rel_list_F=rel_list_G+rel_list_H
			#check if on open list ,if not add to it 
			#check to see if the relative list points are on open list 
			rel_on_open=ismember(rel_list_XY,(open_list_XY))['member_in_a']

			open_on_rel=ismember(rel_list_XY,(open_list_XY))['index_elements_b']
			#only care about indices that are actually found,remove the ones that arent
			open_on_rel=np.delete(open_on_rel,[j for j, x in enumerate(open_on_rel) if x == -1],axis=0)
			open_on_rel=np.matrix(open_on_rel)
			#Which ever point are not on the open list are added to the open list 
			#(in the first avaliable empty slots )

			num_not_on_open=rel_on_open.count(0) 
			emptyOpen=np.where( np.isnan(open_list_XY[:,0])>0)[0]
			
			if num_not_on_open>=1:

				index_not_in_rel_on_open=[i for i, y in enumerate(rel_on_open) if y == 0]
				open_list_XY[emptyOpen[0:num_not_on_open],:]=rel_list_XY[index_not_in_rel_on_open,:].astype(int)
				F_value=rel_list_F[index_not_in_rel_on_open][:,0]
				G_value=rel_list_G[index_not_in_rel_on_open][:,0]
				H_value=rel_list_H[index_not_in_rel_on_open][:,0]

				open_list_FGH[emptyOpen[0:num_not_on_open],:]=np.concatenate( ( np.asarray(F_value), np.asarray(G_value),np.asarray(H_value) ),axis=1)
				open_list_parent[emptyOpen[0:num_not_on_open],0]=current_XY[0].astype(int)
				
				open_list_parent[emptyOpen[0:num_not_on_open],1]=current_XY[1].astype(int)
				#remove the members of the relative list that were just put on the open 
				#list (only care about the F,G and the XY and the H are not updated 
				#past this point )
				rel_list_F=np.delete(rel_list_F,index_not_in_rel_on_open)
				rel_list_G=np.delete(rel_list_G,index_not_in_rel_on_open)
				
			else:
				rel_list_F=np.delete(rel_list_F,[])
				rel_list_G=np.delete(rel_list_G,[])
			#check G cost of rest to see if it should be updated 
			#Find the G value for the same points on the relative list Track 
			#which G values for lower on the relative list 
			
			
			lower_G=np.less(rel_list_G, open_list_FGH[open_on_rel,1]).astype(int)
			
			position_of_one=[]
			for i in range(lower_G.size):
				if lower_G[0,i]==1:
					position_of_one.append(i)
			lowest_G_index=lower_G.size
			position_of_one=np.asarray(position_of_one)
			

			if lowest_G_index > 0 and position_of_one.size >0:

				open_list_FGH[ open_on_rel[0,position_of_one],0]=rel_list_F[0,position_of_one]
				open_list_FGH[ open_on_rel[0,position_of_one],1]=rel_list_G[0,position_of_one]

				open_list_parent[open_on_rel[0,position_of_one],0]=current_XY[0]
				open_list_parent[open_on_rel[0,position_of_one],1]=current_XY[1]

		#Organize list for the search 
		#negate the F and H cost to be able to descending rowsort then negates back to original values 
		open_list_FGH=-1*open_list_FGH
		
		idx=np.argsort(open_list_FGH[:,0], axis=0)
		# print 'open_list_FGH',open_list_FGH
		# print 'idx',idx
		open_list_FGH=-open_list_FGH[idx,:]
		
		open_list_XY=open_list_XY[idx,:]
		
		open_list_parent=open_list_parent[idx,:]


	#if solution is found generate path 
	#Generate path by recursively checking parent values 
	if solution_found:
		#create a list of the path points for organizational purposes for outputting later 
		PathList=Path;
		# PathList=[]
		#set first element as current position (the goal point)
		PathList[0,:]=current_XY
		#Track current squares location on closed list 
		CurrentSquareClosed=emptyClosed[0][0]
		# print 'CurrentSquareClosed',CurrentSquareClosed
		#create an incrementing variable for the path elements 
		next_index=1

		while np.invert(np.all(np.isnan(closed_list_parent[CurrentSquareClosed,:]))):
			PathList[next_index,:]=closed_list_parent[CurrentSquareClosed,:]
			locs_index=ismember(np.matrix(closed_list_parent[CurrentSquareClosed,:]),(closed_list_XY))['index_elements_b']
			CurrentSquareClosed=locs_index[0]
			next_index+=1
		
		PathList=np.delete(PathList,np.where( np.isnan(PathList[:,0]) ),axis=0)
		flipud_PathList_WITHOUT_NAN=np.flipud(PathList)
		Path[0:len(flipud_PathList_WITHOUT_NAN[:,0]),:]=flipud_PathList_WITHOUT_NAN.astype(int)
		index_of_Path=np.delete(Path,np.where( np.isnan(Path[:,0]) ),axis=0).astype(int)

	return {'Path':Path}